import 'package:shared_preferences/shared_preferences.dart';

Future<String> getsubject() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getString("Subject");
}

Future<bool> savesubject(String fullname) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setString("Subject", fullname);
}

Future<bool> getIsLoggedIn() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  bool x = preferences.getBool("login");
  print("getting user loggedin $x");
  return preferences.getBool("login");
}

Future<bool> saveIsLoggedIn(bool isLoggedIn) async {
  print("saving user sLogged In SharedPreferences");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setBool("login", isLoggedIn);
}


Future<bool> isScienceSeleted() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getBool("Science");
}

Future<bool> saveScience(bool isLoggedIn) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setBool("Science", isLoggedIn);
}

Future<bool> ismathsSeleted() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getBool("Math");
}

Future<bool> saveMaths(bool isLoggedIn) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setBool("Math", isLoggedIn);
}


Future<String> getgrade() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getString("Grade");
}

Future<bool> savegrade(String grade) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setString("Grade", grade);
}

Future<int> getcurriculum() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getInt("curriculum");
}

Future<bool> savecurriculum(int grade) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setInt("curriculum", grade);
}

Future<String> getuserid() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getString("userid");
}

Future<bool> saveuserid(String userid) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setString("userid", userid);
}
