import 'dart:convert';
import 'package:c3d_learning/models/TopicModel.dart';
import 'package:c3d_learning/models/VimeoResponse.dart';
import 'package:c3d_learning/models/chaptersModel.dart';
import 'package:c3d_learning/models/checkuser.dart';
import 'package:c3d_learning/models/lessonModel.dart';
import 'package:c3d_learning/ui/Home/mainscreen/chapter/Chapter.dart';
import 'package:c3d_learning/utils/sharedperference.dart';
import 'package:http/http.dart' as http;
import 'package:c3d_learning/models/TrueOrFalse.dart';
import 'package:c3d_learning/models/SignupModel.dart';
import 'package:c3d_learning/utils/Constants.dart';

Map<String, String> headers = {
  'Content-type': 'application/json',
  'Accept': 'application/json',
};

// notusing
// Future<void> leads(String email, String password) async {
//   var map = new Map<String, String>();
//   map['email'] = email;
//   map['phone'] = password;
//   var response =
//       await http.post(Constants.ApiBaseUrl + '/leads.php', body: map);
//   if (response.statusCode == 200) {
//     print("leads response" + response.body);
//     TrueOrFalse trueOrFalse = TrueOrFalse.fromJson(json.decode(response.body));
//     if (trueOrFalse.status == "sucess") {
//       print(" 👍👍👍👍👍👍👍👍");
//     } else {
//       print("something went wrong" + trueOrFalse.status);
//     }
//   } else {
//     throw Exception('Failed');
//   }
// }

Future<bool> signin(String email, String password, int type) async {
  var map = new Map<String, dynamic>();
  map['email'] = email;
  map['usertype'] = type;
  map['password'] = password;
  var data = json.encode(map);
  var response = await http.post(Constants.ApiBaseUrl + '/login',
      headers: headers, body: data);
  print(response.body.toString());
  if (response.statusCode == 200) {
    print("leads response" + response.body);
    SignupModel signupModel = SignupModel.fromJson(json.decode(response.body));
    if (signupModel.status == "sucess") {
      print(" 👍👍👍👍👍👍👍👍");
      // saving userid change userid to res.userid
      print("incoming userid ${signupModel.userid}");
      await saveuserid(signupModel.userid.toString());
      saveIsLoggedIn(true);
      return true;
    } else {
      print("something went wrong" + signupModel.status);
      return false;
    }
  } else {
    print("failed statusCode is not 200");
    return false;
  }
}

Future<bool> signup(
    String name, String email, String password, String type) async {
  print("sending dat athrough sign up");
  int usertype;
  if (type == "Student") {
    usertype = 2;
  } else {
    usertype = 1;
  }
  var map = new Map<String, dynamic>();
  map['firstname'] = name;
  map['lastname'] = name;
  map['email'] = email;
  map['password'] = password;
  map['username'] = email;
  map['usertype'] = usertype;
  map['vkey'] = "vkey";
  map['firebase_id'] = "firebase_id";
  print(map);
  var data = json.encode(map);
  print(data);

  var response = await http.post(Constants.ApiBaseUrl + '/signup',
      headers: headers, body: data);
  if (response.statusCode == 200) {
    print("leads response" + response.body.toString());
    SignupModel signupData = SignupModel.fromJson(json.decode(response.body));
    if (signupData.status == "sucess") {
      print(" 👍👍👍👍👍👍👍👍");
      print(signupData.mgs);
      // saving userid change userid to res.userid
      print("incoming userid ${signupData.userid}");
      await saveuserid(signupData.userid.toString());
      await saveIsLoggedIn(true);
      return true;
    } else {
      print("something went wrong" + signupData.status);
      return false;
    }
  } else {
    throw Exception('Failed');
  }
}

Future<bool> forgotpassword(String email) async {
  var map = new Map<String, String>();
  map['email'] = email;
  var response =
      await http.post(Constants.ApiBaseUrl + '/forgotpassword.php', body: map);
  if (response.statusCode == 200) {
    print("leads response" + response.body);
    TrueOrFalse trueOrFalse = TrueOrFalse.fromJson(json.decode(response.body));
    if (trueOrFalse.status == "sucess") {
      print(" 👍👍👍👍👍👍👍👍");
      return true;
    } else {
      print("something went wrong" + trueOrFalse.status);
      return false;
    }
  } else {
    throw Exception('Failed');
  }
}

Future<bool> checkuser(int topicid) async {
  var map = new Map<String, dynamic>();
  String userid = await getuserid();
  var uid = int.parse(userid);
  // map['userid'] = uid;
  map['userid'] = 12726;
  map['topicid'] = topicid;
  var data = json.encode(map);
  print(data);
  var response = await http.post(Constants.ApiBaseUrl + '/verfytopic',
      headers: headers, body: data);
  if (response.statusCode == 200) {
    print("verfytopic" + response.body);
    Checkuserdatamodel data =
        Checkuserdatamodel.fromJson(json.decode(response.body));
    if (data.data == 1) {
      print(" 👍👍👍👍👍👍👍👍");
      return true;
    } else {
      print("something went wrong" + data.status);
      return false;
    }
  } else {
    throw Exception('Failed');
  }
}

Future<List<Chapterdata>> chapters(
    bool isscience, bool mathsvalue, String userid) async {
  print("tring to call chapter data");
  var uid = int.parse(userid);
  var map = new Map<String, dynamic>();
  int curriculumid = await getcurriculum();
  String gradeid = await getgrade();
  if (curriculumid == 2 || curriculumid == 3) {
    gradeid = "0$gradeid";
  }
  map['userid'] = uid;
  // map['userid'] = 12726;
  map['curriculumid'] = curriculumid;
  map['gradeid'] = gradeid;

  var data = json.encode(map);
  print("chapters api input $data");
  var response = await http.post(Constants.ApiBaseUrl + '/getchaptersnew',
      headers: headers, body: data);
  if (response.statusCode == 200) {
    print(response.body);
    ChapterResponse data = ChapterResponse.fromJson(json.decode(response.body));
    return data.data;
  } else {
    throw Exception('Failed');
  }
}
// getlessonbychapter

Future<List<Lessondata>> lesson(int chapterid) async {
  print("lessondata calling");
  print("chapterid $chapterid ");
  String userid = await getuserid();
  var uid = int.parse(userid);
  int curriculumid = await getcurriculum();
  String gradeid = await getgrade();
  if (curriculumid == 2 || curriculumid == 3) {
    gradeid = "0$gradeid";
  }
  var map = new Map<String, dynamic>();
  map['chapterid'] = chapterid;
  map['gradeid'] = gradeid;
  map['userid'] = uid;
  // map['userid'] = 12726;
  map['curriculumid'] = curriculumid;
  var data = json.encode(map);
  print("lesson api input $data");
  var response = await http.post(Constants.ApiBaseUrl + '/getLessonsnew',
      headers: headers, body: data);
  print(response.body);
  if (response.statusCode == 200) {
    print(response.body);
    LessonResponse data = LessonResponse.fromJson(json.decode(response.body));
    return data.data;
  } else {
    throw Exception('Failed');
  }
}

Future<List<Topicdata>> topics(int lessonid) async {
  print("Topicdata calling");
  String userid = await getuserid();
  var uid = int.parse(userid);
  int curriculumid = await getcurriculum();
  String gradeid = await getgrade();
  if (curriculumid == 2 || curriculumid == 3) {
    gradeid = "0$gradeid";
  }
  var map = new Map<String, dynamic>();
  map['userid'] = uid;
  // map['userid'] = 12726;
  map['curriculumid'] = curriculumid;
  map['gradeid'] = gradeid;
  map['lessonid'] = lessonid;
  var data = json.encode(map);
  print("topics input $data");
  var response = await http.post(Constants.ApiBaseUrl + '/gettopicsnew',
      headers: headers, body: data);
  print(response.body);
  if (response.statusCode == 200) {
    print(response.body);
    TopicResponse data = TopicResponse.fromJson(json.decode(response.body));
    return data.data;
  } else {
    throw Exception('Failed');
  }
}

Future<String> getVimeoUrl(String vimeoUrl) async {
  print("url to be changed is::::::::::::" + vimeoUrl);
  var map = new Map<String, String>();
  map['Content-Type'] = "application/json";
  map['Authorization'] = "Bearer db6ccd38820d6790d4ebacf062d5577a";
  int lastSplashIndex = vimeoUrl.lastIndexOf("/");
  String videoId = vimeoUrl.substring(lastSplashIndex);
  //widget.videoIdGlobal = videoId;
  final response =
      await http.get('https://api.vimeo.com/me/videos/$videoId', headers: map);

  if (response.statusCode == 200) {
    print(response.body);
    VimeoResponse vimeoResponseFromJson =
        VimeoResponse.fromJson(json.decode(response.body));
    var url = "";
    if (vimeoResponseFromJson.download.length != 0) {
      url = vimeoResponseFromJson
          .download[vimeoResponseFromJson.download.length - 1].link;
      //widget.videoThumbnail = vimeoResponseFromJson.pictures.sizes[vimeoResponseFromJson.pictures.sizes.length - 1].link;
    }
    return url;
  } else {
    throw Exception('Failed to load Curriculam');
  }
}
