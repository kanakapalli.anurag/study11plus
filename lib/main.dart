import 'package:c3d_learning/ui/splashScreen/splashscreen.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'C3D Learning',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new Splashscreen(),
    );
  }
}