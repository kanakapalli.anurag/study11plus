// To parse this JSON data, do
//
//     final vimeoResponse = vimeoResponseFromJson(jsonString);

import 'dart:convert';

VimeoResponse vimeoResponseFromJson(String str) => VimeoResponse.fromJson(json.decode(str));

String vimeoResponseToJson(VimeoResponse data) => json.encode(data.toJson());

class VimeoResponse {
  VimeoResponse({
    this.uri,
    this.name,
    this.description,
    this.type,
    this.link,
    this.duration,
    this.width,
    this.language,
    this.height,
    this.embed,
    this.createdTime,
    this.modifiedTime,
    this.releaseTime,
    this.contentRating,
    this.license,
    this.privacy,
    this.pictures,
    this.tags,
    this.stats,
    this.categories,
    this.metadata,
    this.user,
    this.reviewPage,
    this.parentFolder,
    this.lastUserActionEventDate,
    this.files,
    this.download,
    this.app,
    this.status,
    this.resourceKey,
    this.upload,
    this.transcode,
  });

  String uri;
  String name;
  dynamic description;
  String type;
  String link;
  int duration;
  int width;
  dynamic language;
  int height;
  Embed embed;
  DateTime createdTime;
  DateTime modifiedTime;
  DateTime releaseTime;
  List<String> contentRating;
  dynamic license;
  VideosPrivacy privacy;
  Pictures pictures;
  List<dynamic> tags;
  Stats stats;
  List<dynamic> categories;
  VimeoResponseMetadata metadata;
  User user;
  ReviewPage reviewPage;
  ParentFolder parentFolder;
  DateTime lastUserActionEventDate;
  List<Download> files;
  List<Download> download;
  App app;
  String status;
  String resourceKey;
  Upload upload;
  Transcode transcode;

  factory VimeoResponse.fromJson(Map<String, dynamic> json) => VimeoResponse(
    uri: json["uri"],
    name: json["name"],
    description: json["description"],
    type: json["type"],
    link: json["link"],
    duration: json["duration"],
    width: json["width"],
    language: json["language"],
    height: json["height"],
    embed: Embed.fromJson(json["embed"]),
    createdTime: DateTime.parse(json["created_time"]),
    modifiedTime: DateTime.parse(json["modified_time"]),
    releaseTime: DateTime.parse(json["release_time"]),
    contentRating: List<String>.from(json["content_rating"].map((x) => x)),
    license: json["license"],
    privacy: VideosPrivacy.fromJson(json["privacy"]),
    pictures: Pictures.fromJson(json["pictures"]),
    tags: List<dynamic>.from(json["tags"].map((x) => x)),
    stats: Stats.fromJson(json["stats"]),
    categories: List<dynamic>.from(json["categories"].map((x) => x)),
    metadata: VimeoResponseMetadata.fromJson(json["metadata"]),
    user: User.fromJson(json["user"]),
    reviewPage: ReviewPage.fromJson(json["review_page"]),
    parentFolder: ParentFolder.fromJson(json["parent_folder"]),
    lastUserActionEventDate: DateTime.parse(json["last_user_action_event_date"]),
    files: List<Download>.from(json["files"].map((x) => Download.fromJson(x))),
    download: List<Download>.from(json["download"].map((x) => Download.fromJson(x))),
    app: App.fromJson(json["app"]),
    status: json["status"],
    resourceKey: json["resource_key"],
    upload: Upload.fromJson(json["upload"]),
    transcode: Transcode.fromJson(json["transcode"]),
  );

  Map<String, dynamic> toJson() => {
    "uri": uri,
    "name": name,
    "description": description,
    "type": type,
    "link": link,
    "duration": duration,
    "width": width,
    "language": language,
    "height": height,
    "embed": embed.toJson(),
    "created_time": createdTime.toIso8601String(),
    "modified_time": modifiedTime.toIso8601String(),
    "release_time": releaseTime.toIso8601String(),
    "content_rating": List<dynamic>.from(contentRating.map((x) => x)),
    "license": license,
    "privacy": privacy.toJson(),
    "pictures": pictures.toJson(),
    "tags": List<dynamic>.from(tags.map((x) => x)),
    "stats": stats.toJson(),
    "categories": List<dynamic>.from(categories.map((x) => x)),
    "metadata": metadata.toJson(),
    "user": user.toJson(),
    "review_page": reviewPage.toJson(),
    "parent_folder": parentFolder.toJson(),
    "last_user_action_event_date": lastUserActionEventDate.toIso8601String(),
    "files": List<dynamic>.from(files.map((x) => x.toJson())),
    "download": List<dynamic>.from(download.map((x) => x.toJson())),
    "app": app.toJson(),
    "status": status,
    "resource_key": resourceKey,
    "upload": upload.toJson(),
    "transcode": transcode.toJson(),
  };
}

class App {
  App({
    this.name,
    this.uri,
  });

  String name;
  String uri;

  factory App.fromJson(Map<String, dynamic> json) => App(
    name: json["name"],
    uri: json["uri"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "uri": uri,
  };
}

class Download {
  Download({
    this.quality,
    this.type,
    this.width,
    this.height,
    this.expires,
    this.link,
    this.createdTime,
    this.fps,
    this.size,
    this.md5,
    this.publicName,
    this.sizeShort,
  });

  String quality;
  Type type;
  int width;
  int height;
  DateTime expires;
  String link;
  DateTime createdTime;
  dynamic fps;
  int size;
  String md5;
  String publicName;
  String sizeShort;

  factory Download.fromJson(Map<String, dynamic> json) => Download(
    quality: json["quality"],
    type: typeValues.map[json["type"]],
    width: json["width"] == null ? null : json["width"],
    height: json["height"] == null ? null : json["height"],
    expires: json["expires"] == null ? null : DateTime.parse(json["expires"]),
    link: json["link"],
    createdTime: DateTime.parse(json["created_time"]),
    fps: json["fps"],
    size: json["size"],
    md5: json["md5"],
    publicName: json["public_name"],
    sizeShort: json["size_short"],
  );

  Map<String, dynamic> toJson() => {
    "quality": quality,
    "type": typeValues.reverse[type],
    "width": width == null ? null : width,
    "height": height == null ? null : height,
    "expires": expires == null ? null : expires.toIso8601String(),
    "link": link,
    "created_time": createdTime.toIso8601String(),
    "fps": fps,
    "size": size,
    "md5": md5,
    "public_name": publicName,
    "size_short": sizeShort,
  };
}

enum Type { SOURCE, VIDEO_MP4 }

final typeValues = EnumValues({
  "source": Type.SOURCE,
  "video/mp4": Type.VIDEO_MP4
});

class Embed {
  Embed({
    this.buttons,
    this.logos,
    this.title,
    this.playbar,
    this.volume,
    this.speed,
    this.color,
    this.uri,
    this.html,
    this.badges,
  });

  Buttons buttons;
  Logos logos;
  Title title;
  bool playbar;
  bool volume;
  bool speed;
  String color;
  String uri;
  String html;
  Badges badges;

  factory Embed.fromJson(Map<String, dynamic> json) => Embed(
    buttons: Buttons.fromJson(json["buttons"]),
    logos: Logos.fromJson(json["logos"]),
    title: Title.fromJson(json["title"]),
    playbar: json["playbar"],
    volume: json["volume"],
    speed: json["speed"],
    color: json["color"],
    uri: json["uri"],
    html: json["html"],
    badges: Badges.fromJson(json["badges"]),
  );

  Map<String, dynamic> toJson() => {
    "buttons": buttons.toJson(),
    "logos": logos.toJson(),
    "title": title.toJson(),
    "playbar": playbar,
    "volume": volume,
    "speed": speed,
    "color": color,
    "uri": uri,
    "html": html,
    "badges": badges.toJson(),
  };
}

class Badges {
  Badges({
    this.hdr,
    this.live,
    this.staffPick,
    this.vod,
    this.weekendChallenge,
  });

  bool hdr;
  Live live;
  StaffPick staffPick;
  bool vod;
  bool weekendChallenge;

  factory Badges.fromJson(Map<String, dynamic> json) => Badges(
    hdr: json["hdr"],
    live: Live.fromJson(json["live"]),
    staffPick: StaffPick.fromJson(json["staff_pick"]),
    vod: json["vod"],
    weekendChallenge: json["weekend_challenge"],
  );

  Map<String, dynamic> toJson() => {
    "hdr": hdr,
    "live": live.toJson(),
    "staff_pick": staffPick.toJson(),
    "vod": vod,
    "weekend_challenge": weekendChallenge,
  };
}

class Live {
  Live({
    this.streaming,
    this.archived,
  });

  bool streaming;
  bool archived;

  factory Live.fromJson(Map<String, dynamic> json) => Live(
    streaming: json["streaming"],
    archived: json["archived"],
  );

  Map<String, dynamic> toJson() => {
    "streaming": streaming,
    "archived": archived,
  };
}

class StaffPick {
  StaffPick({
    this.normal,
    this.bestOfTheMonth,
    this.bestOfTheYear,
    this.premiere,
  });

  bool normal;
  bool bestOfTheMonth;
  bool bestOfTheYear;
  bool premiere;

  factory StaffPick.fromJson(Map<String, dynamic> json) => StaffPick(
    normal: json["normal"],
    bestOfTheMonth: json["best_of_the_month"],
    bestOfTheYear: json["best_of_the_year"],
    premiere: json["premiere"],
  );

  Map<String, dynamic> toJson() => {
    "normal": normal,
    "best_of_the_month": bestOfTheMonth,
    "best_of_the_year": bestOfTheYear,
    "premiere": premiere,
  };
}

class Buttons {
  Buttons({
    this.like,
    this.watchlater,
    this.share,
    this.embed,
    this.hd,
    this.fullscreen,
    this.scaling,
  });

  bool like;
  bool watchlater;
  bool share;
  bool embed;
  bool hd;
  bool fullscreen;
  bool scaling;

  factory Buttons.fromJson(Map<String, dynamic> json) => Buttons(
    like: json["like"],
    watchlater: json["watchlater"],
    share: json["share"],
    embed: json["embed"],
    hd: json["hd"],
    fullscreen: json["fullscreen"],
    scaling: json["scaling"],
  );

  Map<String, dynamic> toJson() => {
    "like": like,
    "watchlater": watchlater,
    "share": share,
    "embed": embed,
    "hd": hd,
    "fullscreen": fullscreen,
    "scaling": scaling,
  };
}

class Logos {
  Logos({
    this.vimeo,
    this.custom,
  });

  bool vimeo;
  Custom custom;

  factory Logos.fromJson(Map<String, dynamic> json) => Logos(
    vimeo: json["vimeo"],
    custom: Custom.fromJson(json["custom"]),
  );

  Map<String, dynamic> toJson() => {
    "vimeo": vimeo,
    "custom": custom.toJson(),
  };
}

class Custom {
  Custom({
    this.active,
    this.url,
    this.link,
    this.sticky,
  });

  bool active;
  dynamic url;
  dynamic link;
  bool sticky;

  factory Custom.fromJson(Map<String, dynamic> json) => Custom(
    active: json["active"],
    url: json["url"],
    link: json["link"],
    sticky: json["sticky"],
  );

  Map<String, dynamic> toJson() => {
    "active": active,
    "url": url,
    "link": link,
    "sticky": sticky,
  };
}

class Title {
  Title({
    this.name,
    this.owner,
    this.portrait,
  });

  String name;
  String owner;
  String portrait;

  factory Title.fromJson(Map<String, dynamic> json) => Title(
    name: json["name"],
    owner: json["owner"],
    portrait: json["portrait"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "owner": owner,
    "portrait": portrait,
  };
}

class VimeoResponseMetadata {
  VimeoResponseMetadata({
    this.connections,
    this.interactions,
  });

  PurpleConnections connections;
  Interactions interactions;

  factory VimeoResponseMetadata.fromJson(Map<String, dynamic> json) => VimeoResponseMetadata(
    connections: PurpleConnections.fromJson(json["connections"]),
    interactions: Interactions.fromJson(json["interactions"]),
  );

  Map<String, dynamic> toJson() => {
    "connections": connections.toJson(),
    "interactions": interactions.toJson(),
  };
}

class PurpleConnections {
  PurpleConnections({
    this.comments,
    this.credits,
    this.likes,
    this.pictures,
    this.texttracks,
    this.related,
    this.recommendations,
    this.albums,
    this.availableAlbums,
    this.versions,
  });

  Albums comments;
  dynamic credits;
  Albums likes;
  Albums pictures;
  Albums texttracks;
  dynamic related;
  dynamic recommendations;
  Albums albums;
  Albums availableAlbums;
  Versions versions;

  factory PurpleConnections.fromJson(Map<String, dynamic> json) => PurpleConnections(
    comments: Albums.fromJson(json["comments"]),
    credits: json["credits"],
    likes: Albums.fromJson(json["likes"]),
    pictures: Albums.fromJson(json["pictures"]),
    texttracks: Albums.fromJson(json["texttracks"]),
    related: json["related"],
    recommendations: json["recommendations"],
    albums: Albums.fromJson(json["albums"]),
    availableAlbums: Albums.fromJson(json["available_albums"]),
    versions: Versions.fromJson(json["versions"]),
  );

  Map<String, dynamic> toJson() => {
    "comments": comments.toJson(),
    "credits": credits,
    "likes": likes.toJson(),
    "pictures": pictures.toJson(),
    "texttracks": texttracks.toJson(),
    "related": related,
    "recommendations": recommendations,
    "albums": albums.toJson(),
    "available_albums": availableAlbums.toJson(),
    "versions": versions.toJson(),
  };
}

class Albums {
  Albums({
    this.uri,
    this.options,
    this.total,
  });

  String uri;
  List<Option> options;
  int total;

  factory Albums.fromJson(Map<String, dynamic> json) => Albums(
    uri: json["uri"],
    options: List<Option>.from(json["options"].map((x) => optionValues.map[x])),
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "uri": uri,
    "options": List<dynamic>.from(options.map((x) => optionValues.reverse[x])),
    "total": total,
  };
}

enum Option { GET, PATCH, POST, DELETE, PUT }

final optionValues = EnumValues({
  "DELETE": Option.DELETE,
  "GET": Option.GET,
  "PATCH": Option.PATCH,
  "POST": Option.POST,
  "PUT": Option.PUT
});

class Versions {
  Versions({
    this.uri,
    this.options,
    this.total,
    this.currentUri,
    this.resourceKey,
  });

  String uri;
  List<Option> options;
  int total;
  String currentUri;
  String resourceKey;

  factory Versions.fromJson(Map<String, dynamic> json) => Versions(
    uri: json["uri"],
    options: List<Option>.from(json["options"].map((x) => optionValues.map[x])),
    total: json["total"],
    currentUri: json["current_uri"],
    resourceKey: json["resource_key"],
  );

  Map<String, dynamic> toJson() => {
    "uri": uri,
    "options": List<dynamic>.from(options.map((x) => optionValues.reverse[x])),
    "total": total,
    "current_uri": currentUri,
    "resource_key": resourceKey,
  };
}

class Interactions {
  Interactions({
    this.watchlater,
    this.report,
    this.viewTeamMembers,
    this.edit,
  });

  Watchlater watchlater;
  Report report;
  Edit viewTeamMembers;
  Edit edit;

  factory Interactions.fromJson(Map<String, dynamic> json) => Interactions(
    watchlater: Watchlater.fromJson(json["watchlater"]),
    report: Report.fromJson(json["report"]),
    viewTeamMembers: Edit.fromJson(json["view_team_members"]),
    edit: Edit.fromJson(json["edit"]),
  );

  Map<String, dynamic> toJson() => {
    "watchlater": watchlater.toJson(),
    "report": report.toJson(),
    "view_team_members": viewTeamMembers.toJson(),
    "edit": edit.toJson(),
  };
}

class Edit {
  Edit({
    this.uri,
    this.options,
  });

  String uri;
  List<Option> options;

  factory Edit.fromJson(Map<String, dynamic> json) => Edit(
    uri: json["uri"],
    options: List<Option>.from(json["options"].map((x) => optionValues.map[x])),
  );

  Map<String, dynamic> toJson() => {
    "uri": uri,
    "options": List<dynamic>.from(options.map((x) => optionValues.reverse[x])),
  };
}

class Report {
  Report({
    this.uri,
    this.options,
    this.reason,
  });

  String uri;
  List<Option> options;
  List<String> reason;

  factory Report.fromJson(Map<String, dynamic> json) => Report(
    uri: json["uri"],
    options: List<Option>.from(json["options"].map((x) => optionValues.map[x])),
    reason: List<String>.from(json["reason"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "uri": uri,
    "options": List<dynamic>.from(options.map((x) => optionValues.reverse[x])),
    "reason": List<dynamic>.from(reason.map((x) => x)),
  };
}

class Watchlater {
  Watchlater({
    this.uri,
    this.options,
    this.added,
    this.addedTime,
  });

  String uri;
  List<Option> options;
  bool added;
  dynamic addedTime;

  factory Watchlater.fromJson(Map<String, dynamic> json) => Watchlater(
    uri: json["uri"],
    options: List<Option>.from(json["options"].map((x) => optionValues.map[x])),
    added: json["added"],
    addedTime: json["added_time"],
  );

  Map<String, dynamic> toJson() => {
    "uri": uri,
    "options": List<dynamic>.from(options.map((x) => optionValues.reverse[x])),
    "added": added,
    "added_time": addedTime,
  };
}

class ParentFolder {
  ParentFolder({
    this.createdTime,
    this.modifiedTime,
    this.lastUserActionEventDate,
    this.name,
    this.privacy,
    this.resourceKey,
    this.uri,
    this.user,
    this.metadata,
  });

  DateTime createdTime;
  DateTime modifiedTime;
  DateTime lastUserActionEventDate;
  String name;
  ParentFolderPrivacy privacy;
  String resourceKey;
  String uri;
  User user;
  ParentFolderMetadata metadata;

  factory ParentFolder.fromJson(Map<String, dynamic> json) => ParentFolder(
    createdTime: DateTime.parse(json["created_time"]),
    modifiedTime: DateTime.parse(json["modified_time"]),
    lastUserActionEventDate: DateTime.parse(json["last_user_action_event_date"]),
    name: json["name"],
    privacy: ParentFolderPrivacy.fromJson(json["privacy"]),
    resourceKey: json["resource_key"],
    uri: json["uri"],
    user: User.fromJson(json["user"]),
    metadata: ParentFolderMetadata.fromJson(json["metadata"]),
  );

  Map<String, dynamic> toJson() => {
    "created_time": createdTime.toIso8601String(),
    "modified_time": modifiedTime.toIso8601String(),
    "last_user_action_event_date": lastUserActionEventDate.toIso8601String(),
    "name": name,
    "privacy": privacy.toJson(),
    "resource_key": resourceKey,
    "uri": uri,
    "user": user.toJson(),
    "metadata": metadata.toJson(),
  };
}

class ParentFolderMetadata {
  ParentFolderMetadata({
    this.connections,
  });

  FluffyConnections connections;

  factory ParentFolderMetadata.fromJson(Map<String, dynamic> json) => ParentFolderMetadata(
    connections: FluffyConnections.fromJson(json["connections"]),
  );

  Map<String, dynamic> toJson() => {
    "connections": connections.toJson(),
  };
}

class FluffyConnections {
  FluffyConnections({
    this.items,
    this.videos,
    this.folders,
  });

  Albums items;
  Albums videos;
  Albums folders;

  factory FluffyConnections.fromJson(Map<String, dynamic> json) => FluffyConnections(
    items: Albums.fromJson(json["items"]),
    videos: Albums.fromJson(json["videos"]),
    folders: Albums.fromJson(json["folders"]),
  );

  Map<String, dynamic> toJson() => {
    "items": items.toJson(),
    "videos": videos.toJson(),
    "folders": folders.toJson(),
  };
}

class ParentFolderPrivacy {
  ParentFolderPrivacy({
    this.view,
  });

  String view;

  factory ParentFolderPrivacy.fromJson(Map<String, dynamic> json) => ParentFolderPrivacy(
    view: json["view"],
  );

  Map<String, dynamic> toJson() => {
    "view": view,
  };
}

class User {
  User({
    this.uri,
    this.name,
    this.link,
    this.location,
    this.gender,
    this.bio,
    this.shortBio,
    this.createdTime,
    this.pictures,
    this.websites,
    this.metadata,
    this.locationDetails,
    this.skills,
    this.availableForHire,
    this.canWorkRemotely,
    this.preferences,
    this.contentFilter,
    this.resourceKey,
    this.account,
  });

  String uri;
  String name;
  String link;
  String location;
  String gender;
  dynamic bio;
  dynamic shortBio;
  DateTime createdTime;
  Pictures pictures;
  List<dynamic> websites;
  UserMetadata metadata;
  LocationDetails locationDetails;
  List<dynamic> skills;
  bool availableForHire;
  bool canWorkRemotely;
  Preferences preferences;
  List<String> contentFilter;
  String resourceKey;
  String account;

  factory User.fromJson(Map<String, dynamic> json) => User(
    uri: json["uri"],
    name: json["name"],
    link: json["link"],
    location: json["location"],
    gender: json["gender"],
    bio: json["bio"],
    shortBio: json["short_bio"],
    createdTime: DateTime.parse(json["created_time"]),
    pictures: Pictures.fromJson(json["pictures"]),
    websites: List<dynamic>.from(json["websites"].map((x) => x)),
    metadata: UserMetadata.fromJson(json["metadata"]),
    locationDetails: LocationDetails.fromJson(json["location_details"]),
    skills: List<dynamic>.from(json["skills"].map((x) => x)),
    availableForHire: json["available_for_hire"],
    canWorkRemotely: json["can_work_remotely"],
    preferences: Preferences.fromJson(json["preferences"]),
    contentFilter: List<String>.from(json["content_filter"].map((x) => x)),
    resourceKey: json["resource_key"],
    account: json["account"],
  );

  Map<String, dynamic> toJson() => {
    "uri": uri,
    "name": name,
    "link": link,
    "location": location,
    "gender": gender,
    "bio": bio,
    "short_bio": shortBio,
    "created_time": createdTime.toIso8601String(),
    "pictures": pictures.toJson(),
    "websites": List<dynamic>.from(websites.map((x) => x)),
    "metadata": metadata.toJson(),
    "location_details": locationDetails.toJson(),
    "skills": List<dynamic>.from(skills.map((x) => x)),
    "available_for_hire": availableForHire,
    "can_work_remotely": canWorkRemotely,
    "preferences": preferences.toJson(),
    "content_filter": List<dynamic>.from(contentFilter.map((x) => x)),
    "resource_key": resourceKey,
    "account": account,
  };
}

class LocationDetails {
  LocationDetails({
    this.formattedAddress,
    this.latitude,
    this.longitude,
    this.city,
    this.state,
    this.neighborhood,
    this.subLocality,
    this.stateIsoCode,
    this.country,
    this.countryIsoCode,
  });

  String formattedAddress;
  dynamic latitude;
  dynamic longitude;
  dynamic city;
  dynamic state;
  dynamic neighborhood;
  dynamic subLocality;
  dynamic stateIsoCode;
  dynamic country;
  dynamic countryIsoCode;

  factory LocationDetails.fromJson(Map<String, dynamic> json) => LocationDetails(
    formattedAddress: json["formatted_address"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    city: json["city"],
    state: json["state"],
    neighborhood: json["neighborhood"],
    subLocality: json["sub_locality"],
    stateIsoCode: json["state_iso_code"],
    country: json["country"],
    countryIsoCode: json["country_iso_code"],
  );

  Map<String, dynamic> toJson() => {
    "formatted_address": formattedAddress,
    "latitude": latitude,
    "longitude": longitude,
    "city": city,
    "state": state,
    "neighborhood": neighborhood,
    "sub_locality": subLocality,
    "state_iso_code": stateIsoCode,
    "country": country,
    "country_iso_code": countryIsoCode,
  };
}

class UserMetadata {
  UserMetadata({
    this.connections,
  });

  TentacledConnections connections;

  factory UserMetadata.fromJson(Map<String, dynamic> json) => UserMetadata(
    connections: TentacledConnections.fromJson(json["connections"]),
  );

  Map<String, dynamic> toJson() => {
    "connections": connections.toJson(),
  };
}

class TentacledConnections {
  TentacledConnections({
    this.albums,
    this.appearances,
    this.categories,
    this.channels,
    this.feed,
    this.followers,
    this.following,
    this.groups,
    this.likes,
    this.membership,
    this.moderatedChannels,
    this.portfolios,
    this.videos,
    this.watchlater,
    this.shared,
    this.pictures,
    this.watchedVideos,
    this.foldersRoot,
    this.folders,
    this.block,
  });

  Albums albums;
  Albums appearances;
  Albums categories;
  Albums channels;
  Edit feed;
  Albums followers;
  Albums following;
  Albums groups;
  Albums likes;
  Edit membership;
  Albums moderatedChannels;
  Albums portfolios;
  Albums videos;
  Albums watchlater;
  Albums shared;
  Albums pictures;
  Albums watchedVideos;
  Edit foldersRoot;
  Albums folders;
  Albums block;

  factory TentacledConnections.fromJson(Map<String, dynamic> json) => TentacledConnections(
    albums: Albums.fromJson(json["albums"]),
    appearances: Albums.fromJson(json["appearances"]),
    categories: Albums.fromJson(json["categories"]),
    channels: Albums.fromJson(json["channels"]),
    feed: Edit.fromJson(json["feed"]),
    followers: Albums.fromJson(json["followers"]),
    following: Albums.fromJson(json["following"]),
    groups: Albums.fromJson(json["groups"]),
    likes: Albums.fromJson(json["likes"]),
    membership: Edit.fromJson(json["membership"]),
    moderatedChannels: Albums.fromJson(json["moderated_channels"]),
    portfolios: Albums.fromJson(json["portfolios"]),
    videos: Albums.fromJson(json["videos"]),
    watchlater: Albums.fromJson(json["watchlater"]),
    shared: Albums.fromJson(json["shared"]),
    pictures: Albums.fromJson(json["pictures"]),
    watchedVideos: Albums.fromJson(json["watched_videos"]),
    foldersRoot: Edit.fromJson(json["folders_root"]),
    folders: Albums.fromJson(json["folders"]),
    block: Albums.fromJson(json["block"]),
  );

  Map<String, dynamic> toJson() => {
    "albums": albums.toJson(),
    "appearances": appearances.toJson(),
    "categories": categories.toJson(),
    "channels": channels.toJson(),
    "feed": feed.toJson(),
    "followers": followers.toJson(),
    "following": following.toJson(),
    "groups": groups.toJson(),
    "likes": likes.toJson(),
    "membership": membership.toJson(),
    "moderated_channels": moderatedChannels.toJson(),
    "portfolios": portfolios.toJson(),
    "videos": videos.toJson(),
    "watchlater": watchlater.toJson(),
    "shared": shared.toJson(),
    "pictures": pictures.toJson(),
    "watched_videos": watchedVideos.toJson(),
    "folders_root": foldersRoot.toJson(),
    "folders": folders.toJson(),
    "block": block.toJson(),
  };
}

class Pictures {
  Pictures({
    this.uri,
    this.active,
    this.type,
    this.sizes,
    this.resourceKey,
  });

  String uri;
  bool active;
  String type;
  List<Size> sizes;
  String resourceKey;

  factory Pictures.fromJson(Map<String, dynamic> json) => Pictures(
    uri: json["uri"] == null ? null : json["uri"],
    active: json["active"],
    type: json["type"],
    sizes: List<Size>.from(json["sizes"].map((x) => Size.fromJson(x))),
    resourceKey: json["resource_key"],
  );

  Map<String, dynamic> toJson() => {
    "uri": uri == null ? null : uri,
    "active": active,
    "type": type,
    "sizes": List<dynamic>.from(sizes.map((x) => x.toJson())),
    "resource_key": resourceKey,
  };
}

class Size {
  Size({
    this.width,
    this.height,
    this.link,
    this.linkWithPlayButton,
  });

  int width;
  int height;
  String link;
  String linkWithPlayButton;

  factory Size.fromJson(Map<String, dynamic> json) => Size(
    width: json["width"],
    height: json["height"],
    link: json["link"],
    linkWithPlayButton: json["link_with_play_button"] == null ? null : json["link_with_play_button"],
  );

  Map<String, dynamic> toJson() => {
    "width": width,
    "height": height,
    "link": link,
    "link_with_play_button": linkWithPlayButton == null ? null : linkWithPlayButton,
  };
}

class Preferences {
  Preferences({
    this.videos,
  });

  Videos videos;

  factory Preferences.fromJson(Map<String, dynamic> json) => Preferences(
    videos: Videos.fromJson(json["videos"]),
  );

  Map<String, dynamic> toJson() => {
    "videos": videos.toJson(),
  };
}

class Videos {
  Videos({
    this.privacy,
  });

  VideosPrivacy privacy;

  factory Videos.fromJson(Map<String, dynamic> json) => Videos(
    privacy: VideosPrivacy.fromJson(json["privacy"]),
  );

  Map<String, dynamic> toJson() => {
    "privacy": privacy.toJson(),
  };
}

class VideosPrivacy {
  VideosPrivacy({
    this.view,
    this.comments,
    this.embed,
    this.download,
    this.add,
  });

  String view;
  String comments;
  String embed;
  bool download;
  bool add;

  factory VideosPrivacy.fromJson(Map<String, dynamic> json) => VideosPrivacy(
    view: json["view"],
    comments: json["comments"],
    embed: json["embed"],
    download: json["download"],
    add: json["add"],
  );

  Map<String, dynamic> toJson() => {
    "view": view,
    "comments": comments,
    "embed": embed,
    "download": download,
    "add": add,
  };
}

class ReviewPage {
  ReviewPage({
    this.active,
    this.link,
  });

  bool active;
  String link;

  factory ReviewPage.fromJson(Map<String, dynamic> json) => ReviewPage(
    active: json["active"],
    link: json["link"],
  );

  Map<String, dynamic> toJson() => {
    "active": active,
    "link": link,
  };
}

class Stats {
  Stats({
    this.plays,
  });

  int plays;

  factory Stats.fromJson(Map<String, dynamic> json) => Stats(
    plays: json["plays"],
  );

  Map<String, dynamic> toJson() => {
    "plays": plays,
  };
}

class Transcode {
  Transcode({
    this.status,
  });

  String status;

  factory Transcode.fromJson(Map<String, dynamic> json) => Transcode(
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
  };
}

class Upload {
  Upload({
    this.status,
    this.link,
    this.uploadLink,
    this.completeUri,
    this.form,
    this.approach,
    this.size,
    this.redirectUrl,
  });

  String status;
  dynamic link;
  dynamic uploadLink;
  dynamic completeUri;
  dynamic form;
  dynamic approach;
  dynamic size;
  dynamic redirectUrl;

  factory Upload.fromJson(Map<String, dynamic> json) => Upload(
    status: json["status"],
    link: json["link"],
    uploadLink: json["upload_link"],
    completeUri: json["complete_uri"],
    form: json["form"],
    approach: json["approach"],
    size: json["size"],
    redirectUrl: json["redirect_url"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "link": link,
    "upload_link": uploadLink,
    "complete_uri": completeUri,
    "form": form,
    "approach": approach,
    "size": size,
    "redirect_url": redirectUrl,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
