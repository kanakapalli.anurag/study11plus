import 'dart:convert';

LessonResponse videoListResponseFromJson(String str) =>
    LessonResponse.fromJson(json.decode(str));

String videoListResponseToJson(LessonResponse data) =>
    json.encode(data.toJson());

class LessonResponse {
  LessonResponse({
    this.result,
    this.data,
  });

  String result;
  List<Lessondata> data;

  factory LessonResponse.fromJson(Map<String, dynamic> json) => LessonResponse(
        result: json["result"],
        data: List<Lessondata>.from(
            json["data"].map((x) => Lessondata.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Lessondata {
  Lessondata({
    this.lessonid,
    this.lesson,
    this.lactualprice,
    this.lprice,
    this.chapterid,
    this.topicsCount,
    this.purchased,
  });

  int lessonid;
  int chapterid;
  String lesson;
  double lactualprice;
  double lprice;
  String topicsCount;
  String purchased;

  factory Lessondata.fromJson(Map<String, dynamic> json) => Lessondata(
        lessonid: json["lesson_id"],
        lesson: json["lesson"],
        lactualprice: json["lactualprice"],
        lprice: json["lprice"],
        chapterid: json["chapter_id"],
        topicsCount: json["TopicsCount"],
        purchased: json["purchased"],
      );

  Map<String, dynamic> toJson() => {
        "lesson_id": chapterid,
        "lesson": lesson,
        "lactualprice": lactualprice,
        "lprice": lprice,
        "chapter_id": chapterid,
        "lessonsCount": topicsCount,
        "purchased": purchased,
      };
}
