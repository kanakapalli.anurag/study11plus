import 'dart:convert';

TopicResponse videoListResponseFromJson(String str) =>
    TopicResponse.fromJson(json.decode(str));

String videoListResponseToJson(TopicResponse data) =>
    json.encode(data.toJson());

class TopicResponse {
  TopicResponse({
    this.result,
    this.data,
  });

  String result;
  List<Topicdata> data;

  factory TopicResponse.fromJson(Map<String, dynamic> json) =>
      TopicResponse(
        result: json["result"],
        data: List<Topicdata>.from(
            json["data"].map((x) => Topicdata.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Topicdata {
  Topicdata({
    this.lessonid,
    this.topicid,
    this.topic,
    this.tactualprice,
    this.tprice,
    this.url,
    this.purchased
  });

  int lessonid;
  int topicid;
  String topic;
  double tactualprice;
  double tprice;
  String url;
  String purchased;

  factory Topicdata.fromJson(Map<String, dynamic> json) => Topicdata(
        lessonid: json["lesson_id"],
        topicid: json["topic_id"],
        topic: json["topic"],
        tactualprice: json["tactualprice"],
        tprice: json["tprice"],
        url: json["url"],
        purchased: json["purchased"],
      );

  Map<String, dynamic> toJson() => {
        "lessonid": lessonid,
        "topicid": topicid,
        "topic": topic,
        "tactualprice": tactualprice,
        "tprice": tprice,
        "url": url,
        "purchased" : purchased
      };
}
