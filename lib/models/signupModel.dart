import 'dart:convert';

SignupModel SignupModelFromJson(String str) =>
    SignupModel.fromJson(json.decode(str));

String SignupModelToJson(SignupModel data) => json.encode(data.toJson());

class SignupModel {
  SignupModel({this.status, this.userid, this.mgs});

  String status;
  int userid;
  String mgs;

  factory SignupModel.fromJson(Map<String, dynamic> json) =>
      SignupModel(status: json["status"], userid: json["userid"], mgs: json["mgs"]);

  Map<String, dynamic> toJson() => {
        "status": status,
        "userid": userid,
        "mgs": mgs
      };
}
