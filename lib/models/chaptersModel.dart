// To parse this JSON data, do
//
//     final ChapterResponse = videoListResponseFromJson(jsonString);

import 'dart:convert';

ChapterResponse videoListResponseFromJson(String str) =>
    ChapterResponse.fromJson(json.decode(str));

String videoListResponseToJson(ChapterResponse data) =>
    json.encode(data.toJson());

class ChapterResponse {
  ChapterResponse({
    this.result,
    this.data,
  });

  String result;
  List<Chapterdata> data;

  factory ChapterResponse.fromJson(Map<String, dynamic> json) =>
      ChapterResponse(
        result: json["result"],
        data: List<Chapterdata>.from(
            json["data"].map((x) => Chapterdata.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Chapterdata {
  Chapterdata({
    this.chapterid,
    this.chapter,
    this.actualprice,
    this.price,
    this.lessonsCount,
    this.purchased,
  });

  int chapterid;
  String chapter;
  double actualprice;
  double price;
  String lessonsCount;
  String purchased;

  factory Chapterdata.fromJson(Map<String, dynamic> json) => Chapterdata(
        chapterid: json["chapter_id"],
        chapter: json["chapter"],
        actualprice: json["actualprice"],
        price: json["price"],
        lessonsCount: json["lessonsCount"],
        purchased: json["purchased"],
      );

  Map<String, dynamic> toJson() => {
        "chapter_id": chapterid,
        "chapter": chapter,
        "actualprice": actualprice,
        "price": price,
        "lessonsCount": lessonsCount,
        "purchased": purchased,
      };
}
