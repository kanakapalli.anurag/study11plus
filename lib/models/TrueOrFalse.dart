import 'dart:convert';

TrueOrFalse TrueOrFalseFromJson(String str) => TrueOrFalse.fromJson(json.decode(str));

String TrueOrFalseToJson(TrueOrFalse data) => json.encode(data.toJson());

class TrueOrFalse {
  TrueOrFalse({
    this.status,
  });

  String status;

  factory TrueOrFalse.fromJson(Map<String, dynamic> json) => TrueOrFalse(
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
  };
}


