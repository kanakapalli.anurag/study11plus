import 'dart:convert';

Checkuserdatamodel CheckuserdatamodelFromJson(String str) =>
    Checkuserdatamodel.fromJson(json.decode(str));

String CheckuserdatamodelToJson(Checkuserdatamodel data) =>
    json.encode(data.toJson());

class Checkuserdatamodel {
  Checkuserdatamodel({this.status, this.data});

  String status;
  int data;

  factory Checkuserdatamodel.fromJson(Map<String, dynamic> json) =>
      Checkuserdatamodel(
        status: json["status"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data,
      };
}
