import 'package:c3d_learning/models/Studentsmodel.dart';
import 'package:c3d_learning/ui/navDrawer/student/studentedit/EditStudent.dart';
import 'package:flutter/material.dart';

class Students extends StatefulWidget {
  @override
  _StudentsState createState() => new _StudentsState();
}

class _StudentsState extends State<Students> {
  List<Student> locations = [
    Student(
        studentname: 'Europe/London',
        studentemail: 'London',
        studentdetails: 'uk.png'),
    Student(
        studentname: 'Europe/Berlin',
        studentemail: 'Athens',
        studentdetails: 'greece.png'),
    Student(
        studentname: 'Africa/Cairo',
        studentemail: 'Cairo',
        studentdetails: 'egypt.png'),
    Student(
        studentname: 'Africa/Nairobi',
        studentemail: 'Nairobi',
        studentdetails: 'kenya.png'),
    Student(
        studentname: 'America/Chicago',
        studentemail: 'Chicago',
        studentdetails: 'usa.png'),
    Student(
        studentname: 'America/New_York',
        studentemail: 'New York',
        studentdetails: 'usa.png'),
    Student(
        studentname: 'Asia/Seoul',
        studentemail: 'Seoul',
        studentdetails: 'south_korea.png'),
    Student(
        studentname: 'Asia/Jakarta',
        studentemail: 'Jakarta',
        studentdetails: 'indonesia.png'),
  ];

  @override
  Widget build(BuildContext context) {
    
    void handleClick(String value) {
    Navigator.push( context,  MaterialPageRoute(builder: (context) =>
                        EditStudent("locations[index].studentname",
                                    "locations[index].studentemail",
                                    "locations[index].studentdetail")),
  );
}
    return new Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          backgroundColor: Color(0xFF41c8f5),
          title: new Text("Students", textAlign: TextAlign.center),
          actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: handleClick,
            itemBuilder: (BuildContext context) {
              return {'Add Student'}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          child: ListView.builder(
            itemCount: locations.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                    color: Color(0xFF41c8f5),
                    child: ListTile(
                      onTap: () {
                        Navigator.push( context,  MaterialPageRoute(builder: (context) =>
                        EditStudent(locations[index].studentname,
                                    locations[index].studentemail,
                                    locations[index].studentdetails)),
  );
                      },
                      title: Text(locations[index].studentname,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontFamily: "WorkSansSemiBold")),
                      subtitle: Text(
                        locations[index].studentemail,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontFamily: "WorkSansSemiBold"),
                      ),
                    )),
              );
            },
          ),
          
        )
        );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }
}
