import 'package:flutter/material.dart';

class EditStudent extends StatefulWidget {
  String name;
  String email;
  String details;

  EditStudent(this.name, this.email, this.details);
  @override
  _EditStudentState createState() => new _EditStudentState(this.name, this.email, this.details);
}

class _EditStudentState extends State<EditStudent> {
   String name;
  String email;
  String details;
  
  _EditStudentState(this.name, this.email, this.details);
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Text("name : $name \n email : $email \n details : $details")
        ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }
}
