import 'package:flutter/material.dart';

class Comingsonn extends StatefulWidget {

  @override
  _ComingsonnState createState() => new _ComingsonnState();
}

class _ComingsonnState extends State<Comingsonn> {
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Color(0xFF41c8f5),
      body: Center(
        child: Image.asset('assets/img/Comingsoon.png')
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }
}
