import 'package:c3d_learning/ui/onboarding/constants.dart';
import 'package:flutter/cupertino.dart';
class Slider {
  final String sliderImageUrl;
  final String sliderHeading;
  final String sliderSubHeading;

  Slider(
      {@required this.sliderImageUrl,
      @required this.sliderHeading,
      @required this.sliderSubHeading});
}

final sliderArrayList = [
    Slider(
        sliderImageUrl: 'assets/img/login_logo.png',
        sliderHeading: Constantsintroo.SLIDER_HEADING_1,
        sliderSubHeading: Constantsintroo.SLIDER_DESC),
    Slider(
        sliderImageUrl: 'assets/img/login_logo.png',
        sliderHeading: Constantsintroo.SLIDER_HEADING_2,
        sliderSubHeading: Constantsintroo.SLIDER_DESC),
    Slider(
        sliderImageUrl: 'assets/img/login_logo.png',
        sliderHeading: Constantsintroo.SLIDER_HEADING_3,
        sliderSubHeading: Constantsintroo.SLIDER_DESC),
  ];
