import 'package:c3d_learning/ui/onboarding/slider_layout_view.dart';
import 'package:flutter/material.dart';

class Onboarding extends StatefulWidget {

  @override
  _OnboardingState createState() => new _OnboardingState();
}

class _OnboardingState extends State<Onboarding>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: onBordingBody(),
    );
  }
  
Widget onBordingBody() => Container(
        child: SliderLayoutView(),
      );
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }
}
