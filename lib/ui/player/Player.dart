import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:wakelock/wakelock.dart';


class VideoPlayerScreen extends StatefulWidget {
  String title;
  String url;
  bool isLandscape  = false;
  VideoPlayerScreen(this.title,this.url,this.isLandscape);
  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {

  // TargetPlatform _platform;
  VideoPlayerController _videoPlayerController1;

  ChewieController _chewieController;

  void checkVideo(){
    // Implement your calls inside these conditions' bodies :
    if(_videoPlayerController1.value.position == Duration(seconds: 0, minutes: 0, hours: 0)) {
      print('video Started');
    }

    if(_videoPlayerController1.value.position == _videoPlayerController1.value.duration) {
      print('video Ended');
      Navigator.of(context).pop();
    }

  }

  @override
  void initState() {
  print("user ::::::" + widget.url);
    super.initState();
    Wakelock.enable();

    _videoPlayerController1 = VideoPlayerController.network(
        widget.url);
    _videoPlayerController1.addListener(checkVideo);
    _chewieController = ChewieController(
      videoPlayerController: _videoPlayerController1,
      //aspectRatio: 3 / 2,
      aspectRatio: 16/9,
      autoPlay: true,
      looping: false,
      fullScreenByDefault: true,
      allowFullScreen: true,
    );

  }

  @override
  void dispose() {
    Wakelock.disable();
    _videoPlayerController1.dispose();
    _chewieController.dispose();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1D1C22),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Center(
              child: Chewie(
                controller: _chewieController,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
