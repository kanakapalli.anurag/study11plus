
import 'package:easy_web_view/easy_web_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class WebViewScreen extends StatefulWidget {
  final String title;
  final String url;

  WebViewScreen(this.title, this.url);
  @override
  WebViewScreenState createState() =>
      WebViewScreenState(this.title, this.url);
}

class WebViewScreenState extends State<WebViewScreen> {
  String title;
  String url;
  
void allmodes() {
  SystemChrome.setPreferredOrientations([
   DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]);
}
void portraitMode() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  WebViewScreenState(this.title, this.url);

  @override
  void dispose() {    
    portraitMode();
     super.dispose();
  }
  @override
  void initState() {    
    SystemChrome.setEnabledSystemUIOverlays([]);
    allmodes();
    super.initState();
    // Enable hybrid composition.
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    print("URL ::::::::::::::: $url");
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
            body: EasyWebView(
              src: url,
              isHtml: false, // Use Html syntax
              isMarkdown: false, // Use markdown syntax
              convertToWidgets: false,
              onLoaded: () {},
            )            
            ),
      ),
    );
  }
}
