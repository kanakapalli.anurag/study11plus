import 'package:c3d_learning/ui/onboarding/onborading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class Otpscreen extends StatefulWidget {
  // String name;
  // String email;
  // String password;
  // String number;
  // String otp;
  Otpscreen();

  @override
  _OtpscreenState createState() => _OtpscreenState();
}

class _OtpscreenState extends State<Otpscreen> {
  // String name;
  // String email;
  // String password;
  // String number;
  // String otp;
  bool iswrong = false;
  _OtpscreenState();
  @override
  Widget build(BuildContext context) {
    

    TextEditingController otpController = TextEditingController();
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        gradient: new LinearGradient(
            colors: [const Color(0xFF41c8f5), const Color(0xFF134780)],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(1.0, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: Column(
        children: <Widget>[
          Flexible(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 80.0),
                  child: Text(
                    "Verifying your number!",
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, top: 4.0, right: 16.0),
                  child: Text(
                    "Please type the verification code sent to",
                    style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.normal,
                        color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 30.0, top: 2.0, right: 30.0),
                  child: Text(
                    "your phone",
                    style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Image(
                    image: AssetImage('assets/img/otp-icon.png'),
                    height: 120.0,
                    width: 120.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
                  child: Container(
                    height: 55,
                    decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Color(0xFFFFFFFF),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      border: new Border.all(
                        color: Color(0xff36353B),
                        width: 1.0,
                      ),
                    ),
                    child: new TextField(
                      controller: otpController,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black, decorationColor: Colors.white),
                      decoration: new InputDecoration(
                          hintText: 'Enter OTP',
                          fillColor: Color(0xff36353B),
                          border: InputBorder.none,
                          focusColor: Colors.white),
                      keyboardType: TextInputType.number,
                    ),
                  ),
                ),
                iswrong ? Text("wrong otp") : Text(""),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 30.0, top: 10.0, right: 30.0),
                  child: Container(
                    width: 160,
                    height: 42,
                    decoration: BoxDecoration(
                      color: Color(0xFF41c8f5),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    child: FlatButton(
                      child: Text(
                        "Check",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        if(otpController.text.toString() == "123456"){
                        setState(() {                          
                            iswrong = true;
                          
                        });
                        }else{
                            Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    Onboarding()),
                            (Route<dynamic> route) => route is Onboarding);
                        }

                       
                      },
                    ),
                  ),
                ),
              ],
            ),
            flex: 18,
          ),
        ],
      ),
    ));
  }
}

Future<void> signindialog(context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Your already registered'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              // Text('registeried .'),
              // Text('your 8days accses'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Sign in'),
            onPressed: () {},
          ),
        ],
      );
    },
  );
}

Future<void> welcomeDialog(context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Congratulations'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('Welcome to c3dprime .'),
              Text('Enjoy your 8days accses'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Trail'),
            onPressed: () {},
          ),
          TextButton(
            child: Text('Pay & User'),
            onPressed: () {},
          ),
        ],
      );
    },
  );
}
