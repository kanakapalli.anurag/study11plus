import 'package:c3d_learning/ui/Home/Home.dart';
import 'package:c3d_learning/ui/login/login_page.dart';
import 'package:c3d_learning/utils/sharedperference.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => new _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 3,
        navigateAfterSeconds: new LoginPage(),
        title: new Text('Welcome In SplashScreen'),
        image: new Image(image: AssetImage('assets/img/login_logo.png')),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 200.0,
        loaderColor: Colors.red);
  }

  @override
  void initState() {
    super.initState();
    userdetails();
  }

  Future<void> userdetails() async {
    print("hello");
    bool islogin = await getIsLoggedIn();
    print("islogin = $islogin");
    if (islogin == true) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Home()),
          (Route<dynamic> route) => false);
    }
  }
}
