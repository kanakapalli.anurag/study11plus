import 'package:c3d_learning/ui/Home/games/games.dart';
import 'package:c3d_learning/ui/Home/mainscreen/Mainscreen.dart';
import 'package:c3d_learning/ui/Home/sharedVideos/Sharedvideos.dart';
import 'package:c3d_learning/ui/Search/searchscreen.dart';
import 'package:c3d_learning/ui/comingsoon/Comingsoon.dart';
import 'package:c3d_learning/ui/login/login_page.dart';
import 'package:c3d_learning/utils/sharedperference.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  void handleClick(String value) {
    saveIsLoggedIn(false);
    Navigator.pop(context);
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        (Route<dynamic> route) => route is LoginPage);
  }

  var indexx = 0;
  // final pages = [Mainscreen(),Searchscreen(), Games(), Sharedvideos() ];
   final pages = [Mainscreen(), Games(),Comingsonn()];
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF41c8f5),
        centerTitle: true,
        title: Image.asset(
          'assets/img/logo2-02.png',
          fit: BoxFit.contain,
          height: 35,
        ),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: handleClick,
            itemBuilder: (BuildContext context) {
              return {'Sign out'}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
          
        ],
      ),
// commented dwaer for quick relase
      // drawer: new Drawer(
      //   child: new ListView(
      //     children: <Widget>[
      //       new UserAccountsDrawerHeader(
      //         accountEmail: new Text("kanakapalli.anurag@gmail.com"),
      //         accountName: new Text("kanakapalli anurag"),
      //         currentAccountPicture: new GestureDetector(
      //           child: new CircleAvatar(
      //             backgroundImage: new NetworkImage(
      //                 "https://dasf-bg.com/en/wp-content/themes/industry/i/logo.png"),
      //           ),
      //           onTap: () => print("This is your current account."),
      //         ),
      //         decoration: new BoxDecoration(
      //           color: const Color(0xFF41c8f5),
      //           // image: new DecorationImage(
      //           //   image: new NetworkImage("https://img00.deviantart.net/35f0/i/2015/018/2/6/low_poly_landscape__the_river_cut_by_bv_designs-d8eib00.jpg"),
      //           //   fit: BoxFit.fill
      //           // )
      //         ),
      //       ),
      //       new ListTile(
      //           title: new Text("Profile"),
      //           trailing: new Icon(Icons.supervised_user_circle),
      //           onTap: () {
      //             Navigator.pop(context);
      //             Navigator.push(
      //               context,
      //               MaterialPageRoute(builder: (context) => Profile()),
      //             );
      //           }),
      //       new Divider(),
      //       new ListTile(
      //           title: new Text("Reports"),
      //           trailing: new Icon(Icons.report),
      //           onTap: () {
      //             Navigator.pop(context);
      //             Navigator.push(
      //               context,
      //               MaterialPageRoute(builder: (context) => Reports()),
      //             );
      //           }),
      //       new Divider(),
      //       new ListTile(
      //           title: new Text("Students"),
      //           trailing: new Icon(Icons.star_outline),
      //           onTap: () {
      //             Navigator.pop(context);
      //             Navigator.push(
      //               context,
      //               MaterialPageRoute(builder: (context) => Students()),
      //             );
      //           }),
      //       new Divider(),
      //       new ListTile(
      //         title: new Text("Earn"),
      //         trailing: new Icon(Icons.share_rounded),
      //         onTap: () {
      //           Navigator.pop(context);
      //           Navigator.push(
      //             context,
      //             MaterialPageRoute(builder: (context) => Earn()),
      //           );
      //         },
      //       ),
      //       new Divider(),
      //       new ListTile(
      //         title: new Text("Classroom"),
      //         trailing: new Icon(Icons.class_),
      //         onTap: () {
      //           Navigator.pop(context);
      //           Navigator.push(
      //             context,
      //             MaterialPageRoute(builder: (context) => Classroom()),
      //           );
      //         },
      //       ),
      //       new Divider(),
      //       new ListTile(
      //         title: new Text("Sign out"),
      //         trailing: new Icon(Icons.logout),
      //         onTap: () {
      //           saveIsLoggedIn(false);
      //           Navigator.pop(context);
      //           Navigator.pushAndRemoveUntil(
      //               context,
      //               MaterialPageRoute(
      //                   builder: (BuildContext context) => LoginPage()),
      //               (Route<dynamic> route) => route is LoginPage);
      //         },
      //       ),
      //     ],
      //   ),
      // ),
      body: pages[indexx],
      // bottomNavigationBar
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Color(0xFF41c8f5),
        items: <Widget>[
          Icon(Icons.home, size: 30),
          Icon(Icons.games, size: 30),
          Icon(Icons.share, size: 30),
          // comment shared video for quick release.
          // Icon(Icons.compare_arrows, size: 30),
        ],
        onTap: (index) {
          print("index: ${pages[index]}");
          setState(() {
            indexx = index;
          });
        },
      ),
    );
  }
}
