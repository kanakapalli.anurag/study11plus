import 'package:c3d_learning/models/gamesmodel.dart';
import 'package:c3d_learning/ui/webview/webview.dart';
import 'package:flutter/material.dart';

class Games extends StatefulWidget {
  @override
  _GamesState createState() => new _GamesState();
}

class _GamesState extends State<Games> {
  List<GamesModel> games = [
    GamesModel(
        gamename: 'Puzzle 1',
        gamedeatils: 'Jhon and his father are working in the farm, help them find the object hidden there!',
        imageurl:'https://app.c3dlearning.com/Assets/images/puzzle1.png',
        url: 'https://puzzle.cubedigico.com/Puzzle1/'),
    GamesModel(
        gamename: 'Puzzle 2',
        gamedeatils: 'The animals race is going on, find the objects hidden before the race ends!',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle2.png',
        url: 'https://puzzle.cubedigico.com/Puzzle2/'),
    GamesModel(
        gamename: 'Puzzle 3',
        gamedeatils: 'The birds are singing in melody, let\'s see how many hidden objects can you find?',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle3.png',
        url: 'https://puzzle.cubedigico.com/Puzzle3/'),
    GamesModel(
        gamename: 'Puzzle 4',
        gamedeatils: 'Children are playing in garden, don\'t know there are hidden objects. Help them find all.',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle4.png',
        url: 'https://puzzle.cubedigico.com/Puzzle4/'),
    GamesModel(
        gamename: 'Puzzle 5',
        gamedeatils: 'A sunny day in farm, perfect for picnic. Make it more perfect by finding the hidden objects.',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle5.png',
        url: 'https://puzzle.cubedigico.com/Puzzle5/'),
        GamesModel(
        gamename: 'Puzzle 6',
        gamedeatils: 'Can you imagine there can be hidden object in kitchen too. Find them all.',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle6.png',
        url: 'https://puzzle.cubedigico.com/Puzzle6/'),
        GamesModel(
        gamename: 'Puzzle 7',
        gamedeatils: 'The colourful flower shop has lot of hidden objects. How many can you find?',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle7.png',
        url: 'https://puzzle.cubedigico.com/Puzzle7/'),
        GamesModel(
        gamename: 'Puzzle 8',
        gamedeatils: 'Family coming to picnic doesn\'t know there are hidden objects, help them find',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle8.png',
        url: 'https://puzzle.cubedigico.com/Puzzle8/'),
    GamesModel(
        gamename: 'Puzzle 9',
        gamedeatils: 'Animals on air-ballon trip in mountains, find the hidden objects they are carrying.',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle9.png',
        url: 'https://puzzle.cubedigico.com/Puzzle9/'),
    GamesModel(
        gamename: 'Puzzle 10',
        gamedeatils: 'Beaches are amazing and they have hidden objects too, find them all.',
        imageurl:
            'https://app.c3dlearning.com/Assets/images/puzzle10.png',
        url: 'https://puzzle.cubedigico.com/Puzzle10/'),
  ];
  @override
  Widget build(BuildContext context) {
    var _screenWidth = MediaQuery.of(context).size.width;
    var _itemCount = (_screenWidth / 200).ceil();
    return new Scaffold(
      body: Container(
        color: Color(0xFF41c8f5),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10,10,10,20),          
            child: GridView.builder(                
                scrollDirection: Axis.vertical,
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 400,
                    childAspectRatio: (_screenWidth / _itemCount) / 150),
                itemBuilder: (BuildContext context, int index) {
                  return listItem(games[index].gamename, games[index].gamedeatils,games[index].url,games[index].imageurl);
                },
                itemCount: games.length),
          
        ),
      ),
    );
  }

  Widget listItem(String title, String message, String url, String imageurl) {
    return Card(
        child: InkWell(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Image.network(imageurl,),
                  Text(
                    title,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(5,0,5,0),
                    child: Text(message,
          textAlign: TextAlign.center,),
                  ),
                ],
          ),
          onTap:() {
              Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => WebViewScreen(title,url)),
  );
          },
        ),
        
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }
}
