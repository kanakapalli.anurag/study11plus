import 'package:auto_size_text/auto_size_text.dart';
import 'package:c3d_learning/models/chaptersModel.dart';
import 'package:c3d_learning/style/design_course_app_theme.dart';
import 'package:c3d_learning/ui/Home/mainscreen/Lesson_/Lessonscreen.dart';
import 'package:c3d_learning/ui/purchase/course_info_screen.dart';
import 'package:c3d_learning/utils/api.dart';
import 'package:c3d_learning/utils/sharedperference.dart';
import 'package:flutter/material.dart';

class ChapterListView extends StatefulWidget {
  const ChapterListView({Key key, this.callBack}) : super(key: key);

  final Function callBack;
  @override
  ChapterListViewState createState() => ChapterListViewState();
}

class ChapterListViewState extends State<ChapterListView>
    with TickerProviderStateMixin {
  AnimationController animationController;

  bool isscience;
  bool ismaths;
  String gradeis;
  int curriculum = 2;
  String userid;

  Future<void> initfun() async {
    bool sciencevalue = await isScienceSeleted();
    bool mathsvalue = await ismathsSeleted();
    String gradevalue = await getgrade();
    String useridis = await getuserid();
    setState(() {
      isscience = sciencevalue;
      ismaths = mathsvalue;
      gradeis = gradevalue;
      userid = useridis;
    });
  }

  @override
  void initState() {
    initfun();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int count = MediaQuery.of(context).size.width > 480 ? 3 : 2;
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: FutureBuilder<List<Chapterdata>>(
        future: chapters(isscience, ismaths, userid),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            print("snapsho eeeeet");
            print(snapshot.error);
            // print(snapshot.chapters);
            return const SizedBox();
          } else {
            print(snapshot);
            return GridView(
              padding: const EdgeInsets.all(8),
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: List<Widget>.generate(
                snapshot.data.length,
                (int index) {
                  Chapterdata idata = snapshot.data[index];
                  final int count = snapshot.data.length;
                  final Animation<double> animation =
                      Tween<double>(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                      parent: animationController,
                      curve: Interval((1 / count) * index, 1.0,
                          curve: Curves.fastOutSlowIn),
                    ),
                  );
                  animationController.forward();
                  return CategoryView(
                    callback: () {
                      widget.callBack();
                    },
                    data: idata,
                    animation: animation,
                    animationController: animationController,
                  );
                },
              ),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: count,
                mainAxisSpacing: 32.0,
                crossAxisSpacing: 32.0,
                childAspectRatio: 0.8,
              ),
            );
          }
        },
      ),
    );
  }
}

class CategoryView extends StatelessWidget {
  const CategoryView(
      {Key key,
      this.data,
      this.animationController,
      this.animation,
      this.callback})
      : super(key: key);

  final VoidCallback callback;
  final Chapterdata data;
  final AnimationController animationController;
  final Animation<dynamic> animation;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - animation.value), 0.0),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: () {
                print(
                    "title: ${data.chapter} \n lesson: ${data.price} \n title: ${data.lessonsCount} \n");
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Lessonscreen(data.chapterid)),
                );
              },
              child: SizedBox(
                height: 280,
                child: Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFFF8FAFB),
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(16.0)),
                                // border: new Border.all(
                                //     color: DesignCourseAppTheme.notWhite),
                              ),
                              child: Column(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 16, left: 16, right: 16),
                                                child : AutoSizeText(
                                                data.chapter,
                                                style: TextStyle( fontWeight: FontWeight.w600,
                                                fontSize: 16,
                                                letterSpacing: 0.27,
                                                color: DesignCourseAppTheme
                                                    .darkerText,
                                              ),
  minFontSize: 18,
  maxLines: 2,
  overflow: TextOverflow.ellipsis,
),
                                            
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8,
                                                left: 16,
                                                right: 16,
                                                bottom: 8),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  '${data.lessonsCount}',
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.w200,
                                                    fontSize: 12,
                                                    letterSpacing: 0.27,
                                                    color: DesignCourseAppTheme
                                                        .nearlyBlack,
                                                  ),
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      data.purchased != "yes"? InkWell(
                                                        onTap: () {
                                                          print("buy now");
                                                          // CourseInfoScreen()
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder: (context) =>
                                                                    CourseInfoScreen(
                                                                        data)),
                                                          );
                                                        },
                                                        child: Icon(
                                                          Icons
                                                              .shopping_cart_outlined,
                                                          color:
                                                              DesignCourseAppTheme
                                                                  .nearlyBlue,
                                                          size: 50,
                                                        ),
                                                      ):SizedBox(),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 48,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 48,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding:
                            const EdgeInsets.only(top: 24, right: 16, left: 16),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(16.0)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Color(0xFFFFFFFF),
                                  // offset: const Offset(0.0, 0.0),
                                  blurRadius: 0.0),
                            ],
                          ),
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(16.0)),
                            child: AspectRatio(
                                aspectRatio: 1.28,
                                child: Image.asset(
                                    "assets/design_course/interFace3.png")),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
