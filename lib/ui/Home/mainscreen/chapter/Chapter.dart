import 'package:c3d_learning/style/design_course_app_theme.dart';
import 'package:c3d_learning/ui/Home/mainscreen/chapter/popular_course_list_view.dart';
import 'package:c3d_learning/ui/filter/filters_screen.dart';
import 'package:c3d_learning/utils/Constants.dart';
import 'package:material_dialogs/material_dialogs.dart';
import 'package:flutter/material.dart';
import 'package:c3d_learning/utils/sharedperference.dart';
import 'package:material_dialogs/widgets/buttons/icon_button.dart';

class Chapter extends StatefulWidget {
  @override
  _ChapterState createState() => new _ChapterState();
}

class _ChapterState extends State<Chapter> {
  @override
  void initState() {
    initfun();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      color: DesignCourseAppTheme.nearlyWhite,
      child: Scaffold(
        backgroundColor: Color(0xFF41c8f5),
        body: Column(
          children: <Widget>[
            Constants.IsProduction
                ? SizedBox()
                : Text("debuging only width : $width height : $height"),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    children: <Widget>[
                      Flexible(
                        child: getPopularCourseUI(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getPopularCourseUI() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Chapters',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 22,
                  letterSpacing: 0.27,
                  color: DesignCourseAppTheme.darkerText,
                ),
              ),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  focusColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  splashColor: Colors.grey.withOpacity(0.2),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(4.0),
                  ),
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    Navigator.push<dynamic>(
                      context,
                      MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => FiltersScreen(),
                          fullscreenDialog: true),
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'Filter',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.sort, color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 160),
              child: ChapterListView(),
            ),
          )
        ],
      ),
    );
  }

  Future<void> initfun() async {
    String gradevalue = await getgrade();
    int curr = await getcurriculum();
    String x = await getuserid();
    if (gradevalue ==null || x == null || curr == null) {
      Dialogs.materialDialog(
        color: Colors.white,
        msg: 'Please select the filter',
        title: 'ERROR',
        animation: 'assets/img/error.json',
        context: context,
        actions: [
          IconsButton(
            onPressed: () {
              Navigator.push<dynamic>(
                      context,
                      MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) => FiltersScreen(),
                          fullscreenDialog: true),
                    );
            },
            text: 'Filter',
            iconData: Icons.done,
            color: Colors.blue,
            textStyle: TextStyle(color: Colors.white),
            iconColor: Colors.white,
          ),
        ],
      );
    }
  }
}
