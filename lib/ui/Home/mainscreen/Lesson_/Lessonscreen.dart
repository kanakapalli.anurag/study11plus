import 'package:c3d_learning/models/lessonModel.dart';
import 'package:c3d_learning/ui/Home/mainscreen/Lesson_/category_list_view.dart';
import 'package:c3d_learning/utils/sharedperference.dart';
import 'package:flutter/material.dart';
import 'package:c3d_learning/utils/api.dart';
import 'package:flutter/services.dart';

class Lessonscreen extends StatefulWidget {
  final int chapterid;
  Lessonscreen(this.chapterid);
  @override
  _LessonscreenState createState() => new _LessonscreenState();
}

class _LessonscreenState extends State<Lessonscreen> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF41c8f5),
          centerTitle: true,
          title: Text(
            "Lesson",
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Container(
          color: Color(0xFF41c8f5),
          child: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        // listview
                        CategoryListView(widget.chapterid),
                        // CategoryListView(), CategoryListView(),
                        // CategoryListView(), CategoryListView(),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
