import 'package:flutter/material.dart';

import 'chapter/Chapter.dart';

class Mainscreen extends StatefulWidget {

  @override
  _MainscreenState createState() => new _MainscreenState();
}

class _MainscreenState extends State<Mainscreen> {
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Chapter()
      );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }
}
