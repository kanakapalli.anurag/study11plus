import 'package:auto_size_text/auto_size_text.dart';
import 'package:c3d_learning/models/TopicModel.dart';
import 'package:c3d_learning/models/category.dart';
import 'package:c3d_learning/style/design_course_app_theme.dart';
import 'package:c3d_learning/ui/Home/mainscreen/Topic_/topic_purchase.dart';
import 'package:c3d_learning/ui/detailscreen/detailscreen.dart';
import 'package:c3d_learning/ui/player/Player.dart';
import 'package:c3d_learning/ui/purchase/course_info_screen.dart';
import 'package:c3d_learning/utils/api.dart';
import 'package:flutter/material.dart';

class TopicListView extends StatefulWidget {
  int lessonid;
  TopicListView(this.lessonid);

  @override
  _LessonListViewState createState() => _LessonListViewState();
}

class _LessonListViewState extends State<TopicListView>
    with TickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int countitem = MediaQuery.of(context).size.width > 480 ? 3 : 2;
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: FutureBuilder<List<Topicdata>>(
        future: topics(widget.lessonid),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return GridView(
              padding: const EdgeInsets.all(8),
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: List<Widget>.generate(
                snapshot.data.length,
                (int index) {
                  final int count = snapshot.data.length;
                  final Animation<double> animation =
                      Tween<double>(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                      parent: animationController,
                      curve: Interval((1 / count) * index, 1.0,
                          curve: Curves.fastOutSlowIn),
                    ),
                  );
                  animationController.forward();
                  return CategoryView(
                    callback: () {
                      // widget.callBack();
                    },
                    topic: snapshot.data[index],
                    animation: animation,
                    animationController: animationController,
                  );
                },
              ),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: countitem,
                mainAxisSpacing: 32.0,
                crossAxisSpacing: 32.0,
                childAspectRatio: 0.8,
              ),
            );
          }
        },
      ),
    );
  }
}

class CategoryView extends StatelessWidget {
  const CategoryView(
      {Key key,
      this.topic,
      this.animationController,
      this.animation,
      this.callback})
      : super(key: key);

  final VoidCallback callback;
  final Topicdata topic;
  final AnimationController animationController;
  final Animation<dynamic> animation;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - animation.value), 0.0),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: () async {
                print("asdasd");
                bool x = await checkuser(topic.topicid);
                print("checkuser $x");
                if (topic.purchased == "yes") {
                  print("title: ${topic.topic} \n lesson: ${5} \n title: ${topic.tprice} \n");
                  // VideoPlayerScreen(topic.topic, videoUrl, true)
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            Detailscreen(topic)),
                  );
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            TopicPurchase(topic)),
                  );
                }
              },
              child: SizedBox(
                height: 280,
                child: Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFFF8FAFB),
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(16.0)),
                                // border: new Border.all(
                                //     color: DesignCourseAppTheme.notWhite),
                              ),
                              child: Column(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 16, left: 16, right: 16),
                                            child: AutoSizeText(                                              
                                              topic.topic,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 16,
                                                letterSpacing: 0.27,
                                                color: DesignCourseAppTheme
                                                    .darkerText,
                                              ),
                                              maxLines: 2,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8,
                                                left: 16,
                                                right: 16,
                                                bottom: 8),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                // Text(
                                                //   '${topic.lessonCount} lesson',
                                                //   textAlign: TextAlign.left,
                                                //   style: TextStyle(
                                                //     fontWeight: FontWeight.w200,
                                                //     fontSize: 12,
                                                //     letterSpacing: 0.27,
                                                //     color: DesignCourseAppTheme
                                                //         .grey,
                                                //   ),
                                                // ),
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      // Text(
                                                      //   '${category.rating}',
                                                      //   textAlign:
                                                      //       TextAlign.left,
                                                      //   style: TextStyle(
                                                      //     fontWeight:
                                                      //         FontWeight.w200,
                                                      //     fontSize: 18,
                                                      //     letterSpacing: 0.27,
                                                      //     color:
                                                      //         DesignCourseAppTheme
                                                      //             .grey,
                                                      //   ),
                                                      // ),
                                                      InkWell(
                                                        onTap: () {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder: (context) =>
                                                                    TopicPurchase(
                                                                        topic)),
                                                          );
                                                        },
                                                        child: Icon(
                                                          Icons.add_box,
                                                          color:
                                                              DesignCourseAppTheme
                                                                  .nearlyBlue,
                                                          size: 50,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 48,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 48,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding:
                            const EdgeInsets.only(top: 24, right: 16, left: 16),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(16.0)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Color(0xFFFFFFFF),
                                  // offset: const Offset(0.0, 0.0),
                                  blurRadius: 0.0),
                            ],
                          ),
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(16.0)),
                            child: AspectRatio(
                                aspectRatio: 1.28,
                                child: Image.asset(
                                    'assets/design_course/interFace4.png')),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
