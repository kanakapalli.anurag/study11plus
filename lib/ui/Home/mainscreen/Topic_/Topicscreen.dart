import 'package:c3d_learning/style/design_course_app_theme.dart';
import 'package:c3d_learning/ui/Home/mainscreen/Topic_/Topic_list_view.dart';
import 'package:flutter/material.dart';

class Topicscreen extends StatefulWidget {
  final int lessonid;
  Topicscreen(this.lessonid);

  @override
  _LessonscreenState createState() => new _LessonscreenState();
}

class _LessonscreenState extends State<Topicscreen> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF41c8f5),
        centerTitle: true,
        title: Text(
          "Topics",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        color: DesignCourseAppTheme.nearlyWhite,
        child: Scaffold(
          backgroundColor: Color(0xFF41c8f5),
          body: Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      children: <Widget>[
                        Flexible(
                          child: getPopularCourseUI(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getPopularCourseUI() {
    return Padding(
      padding:
          const EdgeInsets.only(top: 0.0, left: 18, right: 16, bottom: 100),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: TopicListView(widget.lessonid),
            ),
          )
        ],
      ),
    );
  }
}
