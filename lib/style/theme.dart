import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Colors {

  const Colors();

  static const Color loginGradientStart = const Color(0xFF41c8f5);
  static const Color loginGradientEnd = const Color(0xFF134780);

  static const primaryGradient = const LinearGradient(
    colors: const [loginGradientStart, loginGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
}

AppBarTheme appBarTheme() {
  return AppBarTheme(
    color: Color(0xFFFFFFFF),
    elevation: 0,
    brightness: Brightness.light,
    iconTheme: IconThemeData(color: Color(0xFF000000)),
    textTheme: TextTheme(
      headline6: TextStyle(color: Color(0XFF8B8B8B), fontSize: 18),
    ),
  );
}